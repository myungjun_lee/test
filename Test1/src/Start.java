import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Start {

	interface IAdd{
		public int Add(int a, int b);
	}

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		//new Thread(() -> {System.out.println("hello!");}).start();
		IAdd func = (a, b) -> a + b;
		System.out.println(func.Add(10, 20));

        // db test
		testJDBC();

	}

	static void testJDBC(){
		Connection conn = null;
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance(); //인스톨 한 mysql드라이버 설정
			conn = DriverManager.getConnection("jdbc:mysql://ec2-52-198-196-67.ap-northeast-1.compute.amazonaws.com:3306/aki_test","aki","54321");
			Statement stmt = conn.createStatement();

			stmt.executeUpdate("insert into userinfo(user_name,user_level,update_date,add_date,enable_flag) values ('xoxo', 5, now(), now(), 0)");
		}catch(SQLException e){
			e.printStackTrace();
			System.out.print("mysql 접속 실패");
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(conn != null){
				try{
					conn.close();
				}catch(SQLException e){
					System.out.println("close 실패");
				}
			}
		}

	}

}
